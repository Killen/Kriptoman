﻿namespace Kriptoman
{
    partial class Messages
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.sendButton = new System.Windows.Forms.Button();
			this.userComboBox = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.messageBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.picChooser = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.recievedBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// sendButton
			// 
			this.sendButton.Location = new System.Drawing.Point(15, 114);
			this.sendButton.Name = "sendButton";
			this.sendButton.Size = new System.Drawing.Size(286, 35);
			this.sendButton.TabIndex = 1;
			this.sendButton.Text = "Send Message";
			this.sendButton.UseVisualStyleBackColor = true;
			this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
			// 
			// userComboBox
			// 
			this.userComboBox.FormattingEnabled = true;
			this.userComboBox.Location = new System.Drawing.Point(15, 87);
			this.userComboBox.Name = "userComboBox";
			this.userComboBox.Size = new System.Drawing.Size(286, 21);
			this.userComboBox.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 71);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(51, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Send To:";
			// 
			// messageBox
			// 
			this.messageBox.Location = new System.Drawing.Point(15, 31);
			this.messageBox.Name = "messageBox";
			this.messageBox.Size = new System.Drawing.Size(568, 20);
			this.messageBox.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 12);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(50, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "Message";
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "Any picture";
			this.openFileDialog1.Title = "Select a picture for the message";
			// 
			// picChooser
			// 
			this.picChooser.Location = new System.Drawing.Point(308, 114);
			this.picChooser.Name = "picChooser";
			this.picChooser.Size = new System.Drawing.Size(275, 35);
			this.picChooser.TabIndex = 6;
			this.picChooser.Text = "Choose Picture";
			this.picChooser.UseVisualStyleBackColor = true;
			this.picChooser.Click += new System.EventHandler(this.picChooser_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(287, 169);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(0, 13);
			this.label3.TabIndex = 7;
			// 
			// recievedBox
			// 
			this.recievedBox.Location = new System.Drawing.Point(15, 194);
			this.recievedBox.Multiline = true;
			this.recievedBox.Name = "recievedBox";
			this.recievedBox.Size = new System.Drawing.Size(565, 120);
			this.recievedBox.TabIndex = 8;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(15, 175);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(103, 13);
			this.label4.TabIndex = 9;
			this.label4.Text = "Recieved messages";
			this.label4.Click += new System.EventHandler(this.label4_Click);
			// 
			// Messages
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(592, 326);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.recievedBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.picChooser);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.messageBox);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.userComboBox);
			this.Controls.Add(this.sendButton);
			this.Name = "Messages";
			this.Text = "Messages";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.ComboBox userComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox messageBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button picChooser;
        private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox recievedBox;
		private System.Windows.Forms.Label label4;
	}
}