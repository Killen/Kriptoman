﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Kriptoman
{
	class Verifier
	{
		public static void SignFolder(String folderPath, X509Certificate2 cert)
		{
			string[] files = Directory.GetFiles(folderPath);
			FileStream fileCountFile = new FileStream(folderPath + '\\' + "messages.list", FileMode.Create);
			foreach (string filename in files)
			{
				fileCountFile.Write(Encoding.Default.GetBytes(filename + '\n'), 0,
					Encoding.Default.GetBytes(filename + '\n').Length);
			}
			fileCountFile.Flush();
			fileCountFile.Close();
			SignFile(folderPath + '\\' + "messages.list", cert);
		}

		public static int VerifyFolder(String folderPath, X509Certificate2 cert)  //certifikat od programa
		{
			if (VerifyFile(folderPath + '\\' + "messages.list", cert))
			{
				int invalidCount = 0;
				FileStream messageList = new FileStream(folderPath + '\\' + "messages.list", FileMode.Open);
				TextReader messageListText = new StreamReader(messageList);
				string[] files = messageListText.ReadToEnd().Split('\n');

				foreach (var file in files)
				{
					file.Replace("\r", "");

					if (!(file.EndsWith(".pfx") || file.EndsWith(".sig") || file.EndsWith(".list") || file == "")) 				
					{
							if (!File.Exists(file))
							invalidCount++;
						
					}
				}
				messageList.Close();
				return invalidCount;
			}
			return -1;
		}


		public static bool VerifyFile(String path, X509Certificate2 cert) //certifikat od aplikacije
		{
			byte[] fileBytes = File.ReadAllBytes(path);
			byte[] sigBytes = File.ReadAllBytes(path + ".sig");
			bool isValid;
			using (RSA rsa = cert.GetRSAPublicKey())
			{
				isValid = rsa.VerifyData(fileBytes, sigBytes, HashAlgorithmName.SHA512, RSASignaturePadding.Pkcs1);
			}

			return isValid;
			//bolje bi bilo implementirati sa tim da cita iz streama umjesto da se sve ucitava u memoriju
		}

		public static void SignFile(String path, X509Certificate2 cert)
		{
			FileStream file = new FileStream(path, FileMode.Open);
			byte[] fileBytes = new byte[file.Length];
			file.Read(fileBytes, 0, (int) file.Length);
			byte[] signBytes = SignBytes(fileBytes, cert);
			FileStream usersSigFile = new FileStream(path + ".sig", FileMode.Create);
			usersSigFile.Write(signBytes, 0, signBytes.Length);
			usersSigFile.Close();
			file.Close();
		}

		public static byte[] SignBytes(byte[] fileBytes, X509Certificate2 certificate) //certificate pripada aplikaciji
		{
			byte[] signature;
			using (RSA rsa = certificate.GetRSAPrivateKey())
			{
				signature = rsa.SignData(fileBytes, HashAlgorithmName.SHA512, RSASignaturePadding.Pkcs1);
			}
			return signature;
		}


		public static byte[] DecryptMessage(byte[] encMessage, X509Certificate2 cert) //cert od korisnika kome je poslan fajl
		{
			RSACryptoServiceProvider certPrivateKey = (RSACryptoServiceProvider) cert.PrivateKey;

			byte[] lenIV = new byte[4];
			byte[] lenKey = new byte[4];

			Array.Copy(encMessage, lenKey, 4);
			Array.Copy(encMessage, 4, lenIV, 0, 4);

			int keyLength = BitConverter.ToInt32(lenKey, 0);
			int IVLength = BitConverter.ToInt32(lenIV, 0);

			byte[] key = new byte[keyLength];
			byte[] IV = new byte[IVLength];
			Array.Copy(encMessage, 8, key, 0, keyLength);
			Array.Copy(encMessage, 8 + keyLength, IV, 0, IVLength);
			byte[] encryptedMessage = new byte[encMessage.Length - keyLength - IVLength - 8];
			Array.Copy(encMessage, keyLength + IVLength + 8,
				encryptedMessage, 0, encMessage.Length - keyLength - IVLength - 8);

			using (Aes aes = new AesManaged())
			{
				aes.IV = IV;
				aes.Mode = CipherMode.CBC;
				aes.KeySize = 128;
				aes.Padding = PaddingMode.Zeros;

				// Decrypt the session key
				RSAPKCS1KeyExchangeDeformatter keyDeformatter = new RSAPKCS1KeyExchangeDeformatter(certPrivateKey);
				aes.Key = keyDeformatter.DecryptKeyExchange(key);

				// Decrypt the message
				using (MemoryStream plaintext = new MemoryStream())
				{
					using (CryptoStream cs = new CryptoStream(plaintext, aes.CreateDecryptor(), CryptoStreamMode.Write))
					{
						cs.Write(encryptedMessage, 0, encryptedMessage.Length);
						cs.FlushFinalBlock();
					}

					string final = Encoding.Default.GetString(plaintext.ToArray());
					if(final.IndexOf('\0')!=-1)
						final = final.Remove(final.IndexOf('\0'));
					return Encoding.Default.GetBytes(final);
				}
				
			}
		}

		public static byte[]
			EncryptMessage(String message, X509Certificate2 userCert) //userCert je certifikat korisnika kojem se salje poruka
		{
			List<byte> finalBytes = new List<byte>();

			RSACryptoServiceProvider rsaPublicKey
				= (RSACryptoServiceProvider) userCert.PublicKey.Key;

			using (Aes aesManaged = new AesManaged())
			{
				aesManaged.KeySize = 128;
				aesManaged.Mode = CipherMode.CBC;
				aesManaged.Padding = PaddingMode.Zeros;


				RSAPKCS1KeyExchangeFormatter
					keyFormatter = new RSAPKCS1KeyExchangeFormatter(rsaPublicKey); //creates a key exchange
				byte[] encKey = keyFormatter.CreateKeyExchange(aesManaged.Key, aesManaged.GetType());

				int lKey = encKey.Length;
				int lIV = aesManaged.IV.Length;

				finalBytes.AddRange(BitConverter.GetBytes(lKey));
				finalBytes.AddRange(BitConverter.GetBytes(lIV));
				finalBytes.AddRange(encKey);
				finalBytes.AddRange(aesManaged.IV);


				ICryptoTransform encryptor = aesManaged.CreateEncryptor();

				using (MemoryStream memory = new MemoryStream())
				{
					using (CryptoStream encryptStream = new CryptoStream(memory, encryptor, CryptoStreamMode.Write))
					{
						encryptStream.Write(Encoding.Default.GetBytes(message), 0, Encoding.Default.GetBytes(message).Length);
						encryptStream.FlushFinalBlock();
						encryptStream.Close();
					}
						finalBytes.AddRange(memory.ToArray());

				}

				return finalBytes.ToArray();

			}
		}

	}
}