﻿namespace Kriptoman
{
	partial class ErrorForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ErrorLabel = new System.Windows.Forms.Label();
			this.OKbutton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// ErrorLabel
			// 
			this.ErrorLabel.AutoSize = true;
			this.ErrorLabel.Location = new System.Drawing.Point(12, 52);
			this.ErrorLabel.Name = "ErrorLabel";
			this.ErrorLabel.Size = new System.Drawing.Size(28, 13);
			this.ErrorLabel.TabIndex = 0;
			this.ErrorLabel.Text = "error";
			this.ErrorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// OKbutton
			// 
			this.OKbutton.Location = new System.Drawing.Point(162, 100);
			this.OKbutton.Name = "OKbutton";
			this.OKbutton.Size = new System.Drawing.Size(75, 25);
			this.OKbutton.TabIndex = 1;
			this.OKbutton.Text = "OK";
			this.OKbutton.UseVisualStyleBackColor = true;
			this.OKbutton.Click += new System.EventHandler(this.OKbutton_Click);
			// 
			// ErrorForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(384, 141);
			this.Controls.Add(this.OKbutton);
			this.Controls.Add(this.ErrorLabel);
			this.Name = "ErrorForm";
			this.Text = "Error";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label ErrorLabel;
		private System.Windows.Forms.Button OKbutton;
	}
}