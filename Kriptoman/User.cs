﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Kriptoman
{
	public class User
	{
		public String username { get; }
		public byte[] passwordHash { get; }
		public String userFolderPath { get; }
		public X509Certificate2 certificate { get; set; }

		public User(String name, byte[] hash, String folderPath, X509Certificate2 cert)
		{
			username = name;
			userFolderPath = folderPath;
			passwordHash = hash;
			certificate = cert ?? throw new ArgumentNullException(nameof(cert));
		}

		public User(String name, String password, String folderPath)
		{
			username = name;
			userFolderPath = folderPath;
			SHA512 hash = new SHA512Managed();
			hash.ComputeHash(Encoding.Unicode.GetBytes(password));
			passwordHash = hash.Hash;
			certificate =
				new X509Certificate2(folderPath + "\\" + name + ".pfx", password,
					X509KeyStorageFlags.PersistKeySet); //zbog ovoga je neophodno da korisnik ima istu sifru za cert i za nalog
		}
	}
}