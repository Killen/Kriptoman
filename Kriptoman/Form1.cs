﻿using System;
using System.Windows.Forms;

#pragma warning disable IDE1006 // Naming Styles

namespace Kriptoman
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
		}
		protected override void OnClosed(EventArgs e)
		{
			base.OnClosed(e);
			if (Application.OpenForms.Count == 1)
			Application.Exit();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			User currUser = Program.Login(userBox.Text, passBox.Text);
			if (currUser != null)
			{
				Messages messageForm = new Messages(currUser);
				messageForm.Show();
				Close();
			}
			else
			{
				var ef = new ErrorForm("Invalid username or password");
				ef.Show();
			}
			
		}
	}
}