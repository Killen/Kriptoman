﻿using System;
using System.Drawing;
using System.IO;
using System.Linq.Expressions;
using System.Text;
using System.Windows.Forms;

#pragma warning disable IDE1006 // Naming Styles


namespace Kriptoman
{
	public partial class Messages : Form
	{
		private User user { get; }
		private Bitmap image { get; set; }

		public Messages(User userObj)
		{
			InitializeComponent();
			user = userObj;
			foreach (var oUser in Program.userList)
			{
				if (user.username != oUser.username)
					userComboBox.Items.Add(oUser.username);
			}

			string[] recieved = Directory.GetFiles(user.userFolderPath);

			try
			{
				int count = Verifier.VerifyFolder(user.userFolderPath, Program.certificate);
				switch (count)
				{
					case -1:
						recievedBox.Text += "Index file has been tampered with. \n";
						break;
					case 0:
						break;
					default:
						recievedBox.Text += "Deleted messages: " + count;
						break;
				}
			}
			catch (Exception)
			{
				recievedBox.Text += "Index file not found, if this is your first login then it's ok. \n";
			}

			foreach (var path in recieved)
			{
				if (path.EndsWith(".pfx") || path.EndsWith(".sig") || path.EndsWith(".list")) continue;
				string message =
					Encoding.Default.GetString(Verifier.DecryptMessage(Stego.RecoverBytes(new Bitmap(path)),
						user.certificate));
				if (!Verifier.VerifyFile(path, Program.GetUserPubCert(message.Remove(message.IndexOf('@')).Trim()))) continue;

				recievedBox.Text += message + '\n';
				File.Delete(path);
				File.Delete(path + ".sig");
			}
		}

		protected override void OnClosed(EventArgs e)
		{
			base.OnClosed(e);
			if (Application.OpenForms.Count == 1)
				Verifier.SignFolder(user.userFolderPath,Program.certificate);
				Application.Exit();
		}

		private void picChooser_Click(object sender, EventArgs e)
		{
			openFileDialog1.ShowDialog();
			if (openFileDialog1.CheckFileExists)
				image = new Bitmap(openFileDialog1.OpenFile());
			else throw new FileNotFoundException();
		}

		private void sendButton_Click(object sender, EventArgs e)
		{
			
			Bitmap resultImage;
			try
			{
				if (user.certificate.NotAfter < DateTime.Now) throw new Exception("Outdated certificate");

				User selectedUser;

				if (messageBox.Text != "" && openFileDialog1.CheckFileExists && userComboBox.SelectedItem.ToString() != "")
				{
					selectedUser = Program.GetUserByName(userComboBox.SelectedItem.ToString());
					string fullMessageText = user.username + "@" + DateTime.Now.ToString(@"dd.MM.yyyy. H:mm:ss  ") + " >> " + messageBox.Text;
					resultImage = Stego.HideBytes(image, Verifier.EncryptMessage(fullMessageText, selectedUser.certificate));
				}
				else throw new Exception("One of the input fields is invalid");

				string destinationPath = selectedUser.userFolderPath + @"\" + openFileDialog1.SafeFileName;
				resultImage.Save(destinationPath);
				Verifier.SignFile(destinationPath, user.certificate);
				Verifier.SignFolder(selectedUser.userFolderPath, Program.certificate);
			}
			catch (Exception err)
			{
				ErrorForm ef = new ErrorForm(err.Message);
				ef.Show();
			}
			
		}

		private void label4_Click(object sender, EventArgs e)
		{
		}
	}
}