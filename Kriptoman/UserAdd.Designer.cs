﻿namespace Kriptoman
{
	partial class UserAdd
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.passBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
			this.selectButton = new System.Windows.Forms.Button();
			this.pathBox = new System.Windows.Forms.TextBox();
			this.repBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(34, 209);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(148, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "Add User";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(188, 209);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(151, 23);
			this.button2.TabIndex = 1;
			this.button2.Text = "Save and Exit";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(34, 61);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(148, 20);
			this.textBox1.TabIndex = 2;
			// 
			// passBox
			// 
			this.passBox.Location = new System.Drawing.Point(34, 104);
			this.passBox.Name = "passBox";
			this.passBox.PasswordChar = '+';
			this.passBox.Size = new System.Drawing.Size(148, 20);
			this.passBox.TabIndex = 3;
			this.passBox.UseSystemPasswordChar = true;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(34, 42);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(55, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "Username";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(34, 88);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(53, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "Password";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(34, 131);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(79, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "User root folder";
			// 
			// selectButton
			// 
			this.selectButton.Location = new System.Drawing.Point(34, 147);
			this.selectButton.Name = "selectButton";
			this.selectButton.Size = new System.Drawing.Size(148, 23);
			this.selectButton.TabIndex = 7;
			this.selectButton.Text = "Select";
			this.selectButton.UseVisualStyleBackColor = true;
			this.selectButton.Click += new System.EventHandler(this.button3_Click);
			// 
			// pathBox
			// 
			this.pathBox.Location = new System.Drawing.Point(34, 177);
			this.pathBox.Name = "pathBox";
			this.pathBox.ReadOnly = true;
			this.pathBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.pathBox.Size = new System.Drawing.Size(305, 20);
			this.pathBox.TabIndex = 8;
			// 
			// repBox
			// 
			this.repBox.Location = new System.Drawing.Point(188, 104);
			this.repBox.Name = "repBox";
			this.repBox.PasswordChar = '+';
			this.repBox.Size = new System.Drawing.Size(151, 20);
			this.repBox.TabIndex = 9;
			this.repBox.UseSystemPasswordChar = true;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(188, 88);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(59, 13);
			this.label4.TabIndex = 10;
			this.label4.Text = "Once more";
			// 
			// UserAdd
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(374, 253);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.repBox);
			this.Controls.Add(this.pathBox);
			this.Controls.Add(this.selectButton);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.passBox);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Name = "UserAdd";
			this.Text = "Add User Form";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox passBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.FolderBrowserDialog folderBrowser;
		private System.Windows.Forms.Button selectButton;
		private System.Windows.Forms.TextBox pathBox;
		private System.Windows.Forms.TextBox repBox;
		private System.Windows.Forms.Label label4;
	}
}