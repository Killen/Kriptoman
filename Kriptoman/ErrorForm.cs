﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kriptoman
{
	public partial class ErrorForm : Form
	{
		public ErrorForm(string error)
		{
			InitializeComponent();
			ErrorLabel.Text = error;
		}

		private void OKbutton_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
