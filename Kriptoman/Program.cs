﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;

namespace Kriptoman
{
	static class Program
	{
		public static readonly X509Certificate2 certificate;			 //app certificate which is used for signing system files 
		private const string userListFile = "..\\..\\users.txt";		 //file with the user data

		private static readonly List<X509Certificate2> userCertificates; //list of public certificates, used for user listing and sending

		public static readonly List<User> userList = new List<User>();   //list of users

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			
			if (ConsistencyCheck())
			{
				var form1 = new Form1();
				form1.Show();
				Application.Run();
			}
			else Application.Exit();
		}
		
		static public User GetUserByName(string username)
		{
			foreach (var user in userList)
			{
				if (user.username == username)
					return user;
			}
			return null;
		}

		static X509Certificate2 GetUserPrivateCert(String path, String password)
		{
			X509Certificate2 userCert = new X509Certificate2(
				path,
				password,
				X509KeyStorageFlags.PersistKeySet);
			return userCert;
		} 

		public static X509Certificate2 GetUserPubCert(String username)
		{
			foreach (var cert in userCertificates)
			{
				string name = cert.SubjectName.Name.Remove(cert.SubjectName.Name.IndexOf(',')).Remove(0, 3);
				if (name == username)
					return cert;
			}
			return null;
		} 

		static Program()
		{
			String[] userArr = Directory.GetFiles("..\\..\\Certificates\\");
			userCertificates = new List<X509Certificate2>();
			certificate =
				new X509Certificate2("..\\..\\ca.pfx", @"*3C5LlN4hd8IrdCY0JagJqoerX5izST",
					X509KeyStorageFlags.Exportable); //da, hard-coded je ali bolje i to nego da je plaintext u nekom folderu 

			try
			{
				X509ChainPolicy chainPolicy = new X509ChainPolicy()
				{
					RevocationMode = X509RevocationMode.NoCheck,
					RevocationFlag = X509RevocationFlag.EntireChain,
					VerificationFlags = X509VerificationFlags.IgnoreInvalidBasicConstraints
				}; //voodoo magic, damn near stupid API

				var chain = new X509Chain {ChainPolicy = chainPolicy};
				foreach (string certPath in userArr)
				{
					X509Certificate2 cert = new X509Certificate2(certPath); //opening the certificate in the Certificates folder
					bool
						isCertificateChainValid =
							chain.Build(cert);	 //checking the trust chain to see if it is valid, requires the CA certificate to be installed in the system under trusted root authorities
					if (!isCertificateChainValid)
					{
						foreach (X509ChainElement chainElement in chain.ChainElements)
						{
							foreach (X509ChainStatus chainStatus in chainElement.ChainElementStatus)
							{
								Debug.WriteLine(chainStatus.StatusInformation);
							}
						}
					} //if it is not valid, this will display why

					if (cert.NotAfter > DateTime.Now && cert.NotBefore < DateTime.Now && isCertificateChainValid)
					{
						userCertificates.Add(cert);
					}
				}
			}
			catch (Exception e)
			{
				ErrorForm ef = new ErrorForm(e.Message);
				ef.Show();
			}

			FileStream usersFile = new FileStream(userListFile, FileMode.Open);
			TextReader tr = new StreamReader(usersFile);
			String userData = tr.ReadToEnd();
			tr.Close();
			usersFile.Close();

			String[] usersDataArr = userData.Split('\n'); //podrazumjeva se da korisnike razdvaja newline
			foreach (var user in usersDataArr)
			{
				String[] data = user.Split(';');		  //podrazumjeva se format username;passHash;userFolderPath
				byte[] hash = Convert.FromBase64CharArray(data[1].ToCharArray(), 0, data[1].Length);
				userList.Add(new User(data[0], hash, data[2].Replace("\r", ""), GetUserPubCert(data[0])));
			}
		}

		private static Boolean ConsistencyCheck()
		{
			bool filesOk = Verifier.VerifyFile(userListFile, certificate);

			return filesOk;
		}

		public static void PrintUsers(List<User> userList)
		{
			FileStream usersFileStream = new FileStream(userListFile, FileMode.Create);
			using (StreamWriter sw = new StreamWriter(usersFileStream))
			{
				foreach (var data in userList)
				{
					string hash64 = Convert.ToBase64String(data.passwordHash);
					sw.WriteLine(data.username + ";" + hash64 + ";" + data.userFolderPath);
				}
			}
		}

		public static User Login(string username, string password)
		{
			foreach (var user in userList)
			{
				SHA512Managed hash = new SHA512Managed();
				hash.ComputeHash(Encoding.Unicode.GetBytes(password));

				if (user.username == username && user.passwordHash.SequenceEqual(hash.Hash))
				{
					user.certificate = new X509Certificate2(user.userFolderPath + "\\" + username + ".pfx", password,
						X509KeyStorageFlags.PersistKeySet);
					return user;
				}
			}
			return null;
		}
	}
}