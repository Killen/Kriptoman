﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Kriptoman
{
	public partial class UserAdd : Form
	{
		public UserAdd()
		{
			InitializeComponent();
		}

		private static List<User> addedUsers = new List<User>();

		private void button3_Click(object sender, EventArgs e)
		{
			using (var fbd = folderBrowser)
			{
				DialogResult result = fbd.ShowDialog();

				if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
				{
					pathBox.Text = fbd.SelectedPath;
				}
				else
				{
					pathBox.Text = "That ain't gonna work.";
				}
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			Program.PrintUsers(addedUsers);
			Application.Exit();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (passBox.Text == repBox.Text)
			{
				addedUsers.Add(new User(textBox1.Text, passBox.Text, folderBrowser.SelectedPath));
			}
		}
	}
}