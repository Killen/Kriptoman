﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Kriptoman
{
	public class Stego
	{
		public static Bitmap HideBytes(Bitmap baseImage, byte[] contentBytes)
		{
			Bitmap stegImage = new Bitmap(baseImage);

			
			byte[] message = new byte[contentBytes.Length + 5]; //4 for length info and one terminator zero for overshoot
			var length = BitConverter.GetBytes(contentBytes.Length);
			length.CopyTo(message, 0);
			contentBytes.CopyTo(message, length.Length);
			message[contentBytes.Length + 4] = 0;

			try
			{
				if (stegImage.Height * stegImage.Width * 3 / 8 < message.Length)
					throw new Exception("Image too small");

				byte[] col = new byte[3] {0, 0, 0};
				int lsbc = 0, pixelCount = 0;

				for (int i = 0, x = 0, y = 0; i < message.Length; i++)		//lsbc is least significant bit count
				{
					byte
						data = message[i];

					for (int j = 0; j < 8; j++, lsbc++)
					{
						if ((lsbc % 3 == 0) && (lsbc > 0))
						{
							var newPixel = Color.FromArgb(col[0], col[1], col[2]);
							stegImage.SetPixel(x, y, newPixel);

							pixelCount++;
							y = pixelCount / stegImage.Width;
							x = pixelCount % stegImage.Width;
							Color pixel = stegImage.GetPixel(x, y);
							col[0] = pixel.R;
							col[1] = pixel.G;
							col[2] = pixel.B;
						}

						var bit = (byte) (data >> (7 - j) & 1);
						col[lsbc % 3] &= 254;
						col[lsbc % 3] |= bit;
					}
				}
				if (lsbc % 3 != 0)
				{
					var newPixel = Color.FromArgb(col[0], col[1], col[2]);
					stegImage.SetPixel(pixelCount % stegImage.Width, pixelCount / stegImage.Width, newPixel);
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}

			return stegImage;
		}

		public static byte[] RecoverBytes(Bitmap stegImage)
		{
			List<byte> hiddenBytes = new List<byte>();
			int byteCount = sizeof(int);

			int lsbc = 0;
			int pixelCount = 0;
			byte[] col = new byte[3];
			Color pixel = stegImage.GetPixel(0, 0);
			col[0] = pixel.R;
			col[1] = pixel.G;
			col[2] = pixel.B;

			for (int i = 0; i < byteCount; i++)
			{
				byte temp = 0;
				for (int j = 0; j < 8; j++, lsbc++)
				{
					if ((lsbc % 3 == 0) && (lsbc > 0))
					{
						pixelCount++;
						var y = pixelCount / stegImage.Width;
						var x = pixelCount % stegImage.Width;
						pixel = stegImage.GetPixel(x, y);
						col[0] = pixel.R;
						col[1] = pixel.G;
						col[2] = pixel.B;
					}
					temp = (byte) ((temp << 1) + col[lsbc % 3] % 2);				//temp shiftuje ulijevo i doda na kraj novi bit
				}
				hiddenBytes.Add(temp);
				if (i == sizeof(int) - 1)
				{
					byteCount = BitConverter.ToInt32(hiddenBytes.ToArray(), 0) + sizeof(int); //sets the number of encrypted bytes
				}
			}

			hiddenBytes.RemoveRange(0, 4);				//removes the count bytes
			stegImage.Dispose();
			return hiddenBytes.ToArray();
		}
		
	}
}