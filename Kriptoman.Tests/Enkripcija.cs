﻿using System;
using System.Text;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kriptoman.Tests
{
	/// <summary>
	/// Summary description for Enkripcija
	/// </summary>
	[TestClass]
	public class Enkripcija
	{
		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get { return testContextInstance; }
			set { testContextInstance = value; }
		}

		#region Additional test attributes

		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//

		#endregion

		[TestMethod]
		public void TestEncryption()
		{
			string testData = "testna poruka";
			string result;

			result = Encoding.Default.GetString(Verifier.DecryptMessage(
				Verifier.EncryptMessage(testData, Program.certificate),
				Program.certificate));

			Assert.AreEqual(testData, result);
		}

		[TestMethod]
		public void TestEncryptionUser()
		{
			string testData = "testna poruka";
			string result;
			X509Certificate2 cert = new X509Certificate2(
				@"C:\Users\HP\source\repos\Kriptoman\Kriptoman\Users\Damir Bajramovic\Damir Bajramovic.pfx", "damir",X509KeyStorageFlags.Exportable);

			result = Encoding.Default.GetString(Verifier.DecryptMessage(
				Verifier.EncryptMessage(testData, cert),
				cert));

			Assert.AreEqual(testData, result);
		}

		
		[TestMethod]
		public void TestEncryptionHuge()
		{
			string testData = "I have looked online for what this exception means in relation to my program but can\'t seem to find a solution or the reason why it\'s happening to my specific program. I have been using the example provided my msdn for encrypting and decrypting an XmlDocument using the Rijndael algorithm. The encryption works fine but when I try to decrypt, I get the following exception:";
			string result;

			result = Encoding.Default.GetString(Verifier.DecryptMessage(
				Verifier.EncryptMessage(testData, Program.certificate),
				Program.certificate));

			Assert.AreEqual(testData, result);
		}

		[TestMethod]
		public void TestEncryptionEmpty()
		{
			string testData = "";
			string result;

			result = Encoding.Default.GetString(Verifier.DecryptMessage(
				Verifier.EncryptMessage(testData, Program.certificate),
				Program.certificate));

			Assert.AreEqual(testData, result);
		}


		[TestMethod]
		public void TestStegoValid()
		{
			string testData = "testni string male duzine";
			string result;

			result = Encoding.Default.GetString(Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), Encoding.Default.GetBytes(testData))));
			Assert.AreEqual(testData, result);
		}

		[TestMethod]
		public void TestStegoValidBig()
		{
			string testData = "I have looked online for what this exception means in relation to my program but can\'t seem to find a solution or the reason why it\'s happening to my specific program. I have been using the example provided my msdn for encrypting and decrypting an XmlDocument using the Rijndael algorithm. The encryption works fine but when I try to decrypt, I get the following exception:";
			string result;

			result = Encoding.Default.GetString(Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), Encoding.Default.GetBytes(testData))));
			Assert.AreEqual(testData, result);
		}

		[TestMethod]
		public void TestStegoByteZero()
		{
			byte[] testData = new byte[1000];
			for (var i = 0;i<testData.Length;i++)
			{
				testData[i] = 0;
			}
			byte[] result;

			result = Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), testData));
			bool pass = testData.SequenceEqual(result);
			Assert.IsTrue(pass);
		}

		[TestMethod]
		public void TestStegoByteOnes()
		{
			byte[] testData = new byte[1000];
			for (var i = 0; i < testData.Length; i++)
			{
				testData[i] = 255;
			}
			byte[] result;

			result = Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), testData));
			bool pass = testData.SequenceEqual(result);
			Assert.IsTrue(pass);
		}

		[TestMethod]
		public void TestStegoByteOneZero()
		{
			byte[] testData = new byte[1000];
			for (var i = 0; i < testData.Length; i++)
			{
				testData[i] = 170;
			}
			byte[] result;

			result = Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), testData));
			bool pass = testData.SequenceEqual(result);
			Assert.IsTrue(pass);
		}
		[TestMethod]
		public void TestStegoByteZeroOne()
		{
			byte[] testData = new byte[1000];
			for (var i = 0; i < testData.Length; i++)
			{
				testData[i] = 70;
			}
			byte[] result;

			result = Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), testData));
			bool pass = testData.SequenceEqual(result);
			Assert.IsTrue(pass);
		}
		[TestMethod]
		public void TestStegoByteSequence()
		{
			byte[] testData = new byte[1000];
			for (var i = 0; i < testData.Length; i++)
			{
				testData[i] = BitConverter.GetBytes(i)[0];
			}
			byte[] result;

			result = Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), testData));
			bool pass = testData.SequenceEqual(result);
			Assert.IsTrue(pass);
		}
		[TestMethod]
		public void TestStegoByteZeroSecondImage()
		{
			byte[] testData = new byte[1000];
			for (var i = 0; i < testData.Length; i++)
			{
				testData[i] = 0;
			}
			byte[] result;

			result = Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), testData));
			bool pass = testData.SequenceEqual(result);
			Assert.IsTrue(pass);
		}

		[TestMethod]
		public void TestStegoByteOnesSecondImage()
		{
			byte[] testData = new byte[1000];
			for (var i = 0; i < testData.Length; i++)
			{
				testData[i] = 255;
			}
			byte[] result;

			result = Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), testData));
			bool pass = testData.SequenceEqual(result);
			Assert.IsTrue(pass);
		}

		[TestMethod]
		public void TestStegoByteOneZeroSecondImage()
		{
			byte[] testData = new byte[1000];
			for (var i = 0; i < testData.Length; i++)
			{
				testData[i] = 170;
			}
			byte[] result;

			result = Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), testData));
			bool pass = testData.SequenceEqual(result);
			Assert.IsTrue(pass);
		}
		[TestMethod]
		public void TestStegoByteZeroOneSecondImage()
		{
			byte[] testData = new byte[1000];
			for (var i = 0; i < testData.Length; i++)
			{
				testData[i] = 70;
			}
			byte[] result;

			result = Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), testData));
			bool pass = testData.SequenceEqual(result);
			Assert.IsTrue(pass);
		}
		[TestMethod]
		public void TestStegoByteSequenceSecondImage()
		{
			byte[] testData = new byte[1000];
			for (var i = 0; i < testData.Length; i++)
			{
				testData[i] = BitConverter.GetBytes(i)[0];
			}
			byte[] result;

			result = Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), testData));
			bool pass = testData.SequenceEqual(result);
			Assert.IsTrue(pass);
		}
		[TestMethod]
		public void TestRoundTripUser()
		{
			string testData = "test";
			string result;
			
			X509Certificate2 cert = new X509Certificate2(
				@"C:\Users\HP\source\repos\Kriptoman\Kriptoman\Users\Damir Bajramovic\Damir Bajramovic.pfx", "damir", X509KeyStorageFlags.Exportable);

			byte[] encData = Verifier.EncryptMessage(testData, cert);
			byte[] recData = Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), encData));
			result = Encoding.Default.GetString(Verifier.DecryptMessage(recData, cert));

			Assert.AreEqual(testData, result);
		}
		[TestMethod]
		public void TestRoundTripUserEmpty()
		{
			string testData = "";
			string result;

			X509Certificate2 cert = new X509Certificate2(
				@"C:\Users\HP\source\repos\Kriptoman\Kriptoman\Users\Damir Bajramovic\Damir Bajramovic.pfx", "damir", X509KeyStorageFlags.Exportable);

			byte[] encData = Verifier.EncryptMessage(testData, cert);
			byte[] recData = Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), encData));
			result = Encoding.Default.GetString(Verifier.DecryptMessage(recData, cert));

			Assert.AreEqual(testData, result);
		}
		[TestMethod]
		public void TestRoundTripUserShort()
		{
			string testData = "test";
			string result;

			X509Certificate2 cert = new X509Certificate2(
				@"C:\Users\HP\source\repos\Kriptoman\Kriptoman\Users\Damir Bajramovic\Damir Bajramovic.pfx", "damir", X509KeyStorageFlags.Exportable);

			byte[] encData = Verifier.EncryptMessage(testData, cert);
			byte[] recData = Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), encData));
			result = Encoding.Default.GetString(Verifier.DecryptMessage(recData, cert));

			Assert.AreEqual(testData, result);
		}
		[TestMethod]
		public void TestRoundTripUserHuge()
		{
			string testData = "I have looked online for what this exception means in relation to my program but can\'t seem to find a solution or the reason why it\'s happening to my specific program. I have been using the example provided my msdn for encrypting and decrypting an XmlDocument using the Rijndael algorithm. The encryption works fine but when I try to decrypt, I get the following exception:111111111";
			string result;

			X509Certificate2 cert = new X509Certificate2(
				@"C:\Users\HP\source\repos\Kriptoman\Kriptoman\Users\Damir Bajramovic\Damir Bajramovic.pfx", "damir", X509KeyStorageFlags.Exportable);

			byte[] encData = Verifier.EncryptMessage(testData, cert);
			byte[] recData = Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test.png"), encData));
			result = Encoding.Default.GetString(Verifier.DecryptMessage(recData, cert));

			Assert.AreEqual(testData, result);
		}

		[TestMethod]
		public void TestRoundTripUserHugeColorful()
		{
			string testData = "I have looked online for what this exception means in relation to my program but can\'t seem to find a solution or the reason why it\'s happening to my specific program. I have been using the example provided my msdn for encrypting and decrypting an XmlDocument using the Rijndael algorithm. The encryption works fine but when I try to decrypt, I get the following exception:";
			string result;

			X509Certificate2 cert = new X509Certificate2(
				@"C:\Users\HP\source\repos\Kriptoman\Kriptoman\Users\Damir Bajramovic\Damir Bajramovic.pfx", "damir", X509KeyStorageFlags.Exportable);

			byte[] encData = Verifier.EncryptMessage(testData, cert);
			byte[] recData = Stego.RecoverBytes(Stego.HideBytes(new System.Drawing.Bitmap(@"..\..\test1.png"), encData));
			result = Encoding.Default.GetString(Verifier.DecryptMessage(recData, cert));

			Assert.AreEqual(testData, result);
		}

		[TestMethod]
		public void TestRoundTripUserHugeSaveJustRight()
		{
			string testData = "I have looked online for what this exception means in relation to my program but can\'t seem to find a solution or the reason why it\'s happening to my specific program. I have been using the example provided my msdn for encrypting and decrypting an XmlDocument using the Rijndael algorithm. The encryption works fine but when I try to decrypt, I get the following exc";
			string result;
			Bitmap bmp = new System.Drawing.Bitmap(@"..\..\test.png");
			Bitmap steg;
			X509Certificate2 cert = new X509Certificate2(
				@"C:\Users\HP\source\repos\Kriptoman\Kriptoman\Users\Damir Bajramovic\Damir Bajramovic.pfx", "damir", X509KeyStorageFlags.Exportable);

			byte[] encData = Verifier.EncryptMessage(testData, cert);

			steg = Stego.HideBytes(bmp, encData);
			steg.Save(@"..\..\steg.png");
			Bitmap loadSteg = new Bitmap(@"..\..\steg.png");
			byte[] recData = Stego.RecoverBytes(loadSteg);	
			result = Encoding.Default.GetString(Verifier.DecryptMessage(recData, cert));

			Assert.AreEqual(testData, result);
		}

		[TestMethod]
		public void TestRoundTripUserSaveShort()
		{
			string testData = "test";
			string result;
			Bitmap bmp = new System.Drawing.Bitmap(@"..\..\test.png");
			Bitmap steg;
			X509Certificate2 cert = new X509Certificate2(
				@"C:\Users\HP\source\repos\Kriptoman\Kriptoman\Users\Igor Pejakovic\Igor Pejakovic.pfx", "igor", X509KeyStorageFlags.Exportable);

			byte[] encData = Verifier.EncryptMessage(testData, cert);

			steg = Stego.HideBytes(bmp, encData);
			steg.Save(@"..\..\steg.png");
			Bitmap loadSteg = new Bitmap(@"..\..\steg.png");
			byte[] recData = Stego.RecoverBytes(loadSteg);
			result = Encoding.Default.GetString(Verifier.DecryptMessage(recData, cert));

			Assert.AreEqual(testData, result);
		}

		
		[TestMethod]
		public void TestRoundTripUserHugeSave()
		{
			string testData = "I have looked online for what this exception means in relation to my program but can\'t seem to find a solution or the reason why it\'s happening to my specific program. I have been using the example provided my msdn for encrypting and decrypting an XmlDocument using the Rijndael algorithm. The encryption works fine but when I try to decrypt, I get the following exception:";
			string result;
			Bitmap bmp = new System.Drawing.Bitmap(@"..\..\test.png");
			Bitmap steg;
			X509Certificate2 cert = new X509Certificate2(
				@"C:\Users\HP\source\repos\Kriptoman\Kriptoman\Users\Damir Bajramovic\Damir Bajramovic.pfx", "damir", X509KeyStorageFlags.Exportable);

			byte[] encData = Verifier.EncryptMessage(testData, cert);

			steg = Stego.HideBytes(bmp, encData);
			steg.Save(@"..\..\steg.png");
			Bitmap loadSteg = new Bitmap(@"..\..\steg.png");
			byte[] recData = Stego.RecoverBytes(loadSteg);
			result = Encoding.Default.GetString(Verifier.DecryptMessage(recData, cert));

			Assert.AreEqual(testData, result);
		}

	}
}
